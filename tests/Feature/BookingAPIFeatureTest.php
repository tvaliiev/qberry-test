<?php

namespace Tests\Feature;

use App\Models\FreezerBooking;
use App\Models\Location;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\DatabaseSeeder;
use Database\Seeders\LocationSeeder;
use Database\Seeders\TestLocationsSeeder;
use Database\Seeders\TestUserSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class BookingAPIFeatureTest extends TestCase
{
    use DatabaseMigrations;

    public function testAddingCorrectBooking(): void
    {
        $user = $this->seedAndAuthorize();

        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 12,
            'start_date' => '2022-03-20',
            'area' => 5,
        ])
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) => $json
                ->has('data', fn(AssertableJson $dataJson) => $dataJson
                    ->hasAll([
                        'id',
                        'user_code',
                        'updated_at',
                        'created_at',
                    ])
                    ->whereAll([
                        'location_id' => 1,
                        'start_date' => '2022-03-20',
                        'end_date' => '2022-04-01',
                        'user_id' => $user->id,
                        'capacity' => 3,
                    ])
                )

            );
    }

    public function testFreezerOverflowAfterSuccessfulBooking(): void
    {
        $this->seedAndAuthorize();

        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 12,
            'start_date' => '2022-03-20',
            'area' => 5,
        ])->assertOk();

        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 12,
            'start_date' => '2022-03-20',
            'area' => 5,
        ])
            ->assertInvalid(['start_date' => 'Not having enough room in freezers for this date']);
    }

    public function testFreezerOverflowWhenOrderingBigArea(): void
    {
        $this->seedAndAuthorize();
        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 12,
            'start_date' => '2022-03-20',
            'area' => 20,
        ])
            ->assertInvalid(['start_date' => 'Not having enough room in freezers for this date']);
    }

    public function testThatYouCanOrderForDifferentDate(): void
    {
        $this->seedAndAuthorize();
        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 15,
            'start_date' => '2022-03-20',
            'area' => 10,
        ])
            ->assertOk();

        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 15,
            'start_date' => '2022-04-05',
            'area' => 10,
        ])
            ->assertOk();
    }

    public function testYouCanOrderForDifferentLocationsOnSameDate(): void
    {
        $this->seedAndAuthorize();
        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 1,
            'duration' => 15,
            'start_date' => '2022-03-20',
            'area' => 10,
        ])
            ->assertOk();

        $this->postJson('/api/v1/bookings', [
            'temperature' => -5,
            'location_id' => 2,
            'duration' => 15,
            'start_date' => '2022-03-20',
            'area' => 10,
        ])
            ->assertOk();
    }

    public function seedAndAuthorize(): User
    {
        Carbon::setTestNow('2022-03-20 13:43');
        $this->seed([TestLocationsSeeder::class, TestUserSeeder::class]);
        $user = User::first();
        $this->actingAs($user);
        return $user;
    }
}
