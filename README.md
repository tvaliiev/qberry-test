### Test project for QBerry
Standard laravel setup with Sail. Make sure to run migrations and seeders. 

Token for authorization can be obtained via `/api/v1/token` and should be put in Authorization header.

Api docs can be accessed on `/api/documentation`. There is only single endpoint working which is booking freezer for specific date and duration, but i wrote documentation for other endpoints that will be useful shortly.
