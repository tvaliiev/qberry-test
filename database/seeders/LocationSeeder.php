<?php

namespace Database\Seeders;

use App\Models\FreezerRoom;
use App\Models\Location;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $predefinedLocations = [
            ['city' => 'Уилмингтон (Северная Каролина)'],
            ['city' => 'Торонто'],
            ['city' => 'Портленд (Орегон)'],
            ['city' => 'Варшава'],
            ['city' => 'Валенсия'],
            ['city' => 'Шанхай'],
        ];
        foreach ($predefinedLocations as $location) {
            /** @var Location $locationModel */
            $locationModel = Location::create($location);
            FreezerRoom::factory(random_int(2, 5), [
                'location_id' => $locationModel->getKey(),
            ])->create();
        }
    }
}
