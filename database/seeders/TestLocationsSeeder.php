<?php

namespace Database\Seeders;

use App\Models\FreezerRoom;
use App\Models\Location;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TestLocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $predefinedLocations = [
            [
                'id' => 1,
                'city' => 'Уилмингтон (Северная Каролина)',
                'rooms' => [
                    ['id' => 1, 'temperature' => -1, 'capacity' => 5,],
                    ['id' => 2, 'temperature' => -2, 'capacity' => 5,],
                    ['id' => 3, 'temperature' => -3, 'capacity' => 5,],
                ],
            ],
            [
                'id' => 2,
                'city' => 'Торонто',
                'rooms' => [
                    ['id' => 4, 'temperature' => -3, 'capacity' => 5,],
                    ['id' => 5, 'temperature' => -2, 'capacity' => 5,],
                    ['id' => 6, 'temperature' => -1, 'capacity' => 5,],
                ],
            ],
            [
                'id' => 3,
                'city' => 'Портленд (Орегон)',
                'rooms' => [
                    ['id' => 7, 'temperature' => -7, 'capacity' => 5,],
                    ['id' => 8, 'temperature' => -8, 'capacity' => 5,],
                    ['id' => 9, 'temperature' => -9, 'capacity' => 5,],
                ],
            ],
        ];
        foreach ($predefinedLocations as $location) {
            /** @var Location $locationModel */
            $locationModel = Location::create(collect($location)->only('id', 'city')->toArray());
            FreezerRoom::factory()->createMany(array_map(function ($room)use($locationModel) {
                $room['location_id'] = $locationModel->getKey();
                return $room;
            }, $location['rooms']));
        }
    }
}
