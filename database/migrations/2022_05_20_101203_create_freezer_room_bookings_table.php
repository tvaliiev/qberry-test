<?php

use App\Models\FreezerBooking;
use App\Models\FreezerRoom;
use App\Models\Location;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freezer_room_bookings', function (Blueprint $table) {
            $table->id();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('capacity' )->comment('Booked capacity in blocks');
            $table->foreignIdFor(User::class)->cascadeOnDelete();
            $table->foreignIdFor(FreezerRoom::class)->cascadeOnDelete();
            $table->foreignIdFor(Location::class)->cascadeOnDelete();
            $table->foreignIdFor(FreezerBooking::class)->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freezer_room_bookings');
    }
};
