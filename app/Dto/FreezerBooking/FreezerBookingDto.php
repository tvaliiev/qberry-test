<?php

namespace App\Dto\FreezerBooking;

use Spatie\DataTransferObject\DataTransferObject;

class FreezerBookingDto extends DataTransferObject
{
    public int $id;

    public string $user_code;

    public string $start_date;

    public string $end_date;

    public int $capacity;

    public int $user_id;

    public int $location_id;

    public string $created_at;

    public string $updated_at;
}
