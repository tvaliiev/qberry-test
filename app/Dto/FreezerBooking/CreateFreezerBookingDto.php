<?php

namespace App\Dto\FreezerBooking;

use Spatie\DataTransferObject\DataTransferObject;

class CreateFreezerBookingDto extends DataTransferObject
{
    public int $user_id;
    public int $location_id;
    public string $start_date;
    public string $end_date;
    public int $capacity;
    public string $user_code;
}
