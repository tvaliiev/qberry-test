<?php

namespace App\Dto\FreezerBooking;

use Spatie\DataTransferObject\DataTransferObject;

class CreateFreezerBookingRequestDto extends DataTransferObject
{
    public int $temperature;

    public int $location_id;

    public int $duration;

    public string $start_date;

    public int $area;
}
