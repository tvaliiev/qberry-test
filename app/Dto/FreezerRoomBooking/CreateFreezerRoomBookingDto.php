<?php

namespace App\Dto\FreezerRoomBooking;

use Spatie\DataTransferObject\DataTransferObject;

class CreateFreezerRoomBookingDto extends DataTransferObject
{
    public int $user_id;
    public int $capacity;
    public string $start_date;
    public string $end_date;
    public int $freezer_room_id;
    public int $location_id;
    public int $freezer_booking_id;
}
