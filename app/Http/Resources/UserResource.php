<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="Array of users resource",
 *     schema="UsersResource",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *         type="array",
 *         @OA\Items(ref="#/components/schemas/User"),
 *     ),
 * )
 * @OA\Schema(
 *     title="Single user resouce",
 *     schema="UserResource",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *         ref="#/components/schemas/User",
 *     ),
 * )
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
