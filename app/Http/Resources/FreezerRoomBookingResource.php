<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @OA\Schema(
 *     title="Booking room resouce",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *         type="array",
 *          @OA\Items(ref="#/components/schemas/FreezerRoomBooking")
 *     ),
 * )
 */
class FreezerRoomBookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
