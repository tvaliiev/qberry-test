<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="Booking multiple resouces",
 *     schema="FreezerBookingsResource",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *         type="array",
 *         @OA\Items(ref="#/components/schemas/FreezerBooking"),
 *     ),
 * )
 * @OA\Schema(
 *     title="Booking single resouce",
 *     schema="FreezerBookingResource",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *         ref="#/components/schemas/FreezerBooking",
 *     ),
 * )
 */
class FreezerBookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
