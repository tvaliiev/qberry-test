<?php

namespace App\Http\Controllers;

use OpenApi\Annotations\Property;

class AuthController
{
    /**
     * @OA\Post(
     *      path="/api/v1/auth/login",
     *      operationId="authUser",
     *      tags={"Users", "Authentication"},
     *      summary="Authenticate user by email and password",
     *      description="Returns success",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   title="email",
     *                   property="email",
     *                   description="Email",
     *                   format="email",
     *                   example="john@example.com"
     *               ),
     *              @OA\Property(
     *                  title="password",
     *                  property="password",
     *                  description="password",
     *                  example="pass123"
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  title="data",
     *                  property="data",
     *                  description="Data wrapper",
     *                  @OA\Property(
     *                      title="token",
     *                      property="token",
     *                      description="token",
     *                      example="1|fwefwelkfjwelfkj"
     *                  ),
     *              ),
     *          ),
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="No such user",
     *      ),
     * )
     * @return void
     */
    public function auth(){}

}
