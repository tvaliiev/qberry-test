<?php

namespace App\Http\Controllers;

use App\Dto\FreezerBooking\CreateFreezerBookingRequestDto;
use App\Http\Requests\StoreFreezerBookingRequest;
use App\Http\Requests\UpdateFreezerBookingRequest;
use App\Http\Resources\FreezerBookingResource;
use App\Models\FreezerBooking;
use App\Services\FreezerBookingService;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations\Property;

class FreezerBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @OA\Get(
     *      path="/api/v1/bookings",
     *      operationId="listBookings",
     *      tags={"Booking"},
     *      summary="List your bookings",
     *      description="Returns success",
     *      @OA\Parameter(in="query", name="start_date", description="", required=false, @OA\Schema(format="date")),
     *      @OA\Parameter(in="query", name="end_date", description="", required=false, @OA\Schema(format="date")),
     *      @OA\Parameter(in="query", name="payment_status", description="", required=false, @OA\Schema(ref="#/components/schemas/FreezerBookingPaymentStatus")),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FreezerBookingsResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     * @OA\Get(
     *      path="/api/v1/bookings/by-id/{id}",
     *      operationId="getBookingById",
     *      tags={"Booking"},
     *      summary="Get single booking by id",
     *      description="Returns success",
     *      @OA\Parameter(in="path", name="id", description="ID of booking that you want to show", required=true, example=12, @OA\Schema(type="integer")),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FreezerBookingResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      ),
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function getById()
    {
    }

    /**
     * Display a listing of the resource.
     * @OA\Get(
     *      path="/api/v1/bookings/by-code/{code}",
     *      operationId="getBookingByCode",
     *      tags={"Booking"},
     *      summary="Get single booking by code",
     *      description="Returns success",
     *      @OA\Parameter(in="path", name="code", description="code of booking that you want to show", required=true, example=12, @OA\Schema(type="integer")),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FreezerBookingResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      ),
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function getByCode()
    {
    }

    /**
     * @OA\Get(
     *     path="/api/v1/bookings/availability",
     *     operationId="calculateAvailabilityBooking",
     *     tags={"Booking"},
     *     summary="Calculate when user can book",
     *     description="Returns success",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   property="temperature",
     *                   title="temperature",
     *                   description="Needed temperature for freezer in Celsius",
     *                   format="int64",
     *                   example="-20"
     *               ),
     *               @OA\Property(
     *                   property="duration",
     *                   title="duration",
     *                   description="Duration of booking in days",
     *                   format="int64",
     *                   example="15"
     *               ),
     *               @OA\Property(
     *                   property="area",
     *                   description="Area that you want to book",
     *                   format="int64",
     *                   example="10"
     *               ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="data",
     *                 title="data",
     *                 description="Data wrapper",
     *                 type="array",
     *                 @OA\Items(type="string", format="date")
     *             ),
     *         )
     *      ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *     ),
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function getAvailableDates()
    {
    }

    /**
     * @OA\Post(
     *      path="/api/v1/bookings",
     *      operationId="createBookings",
     *      tags={"Booking"},
     *      summary="Book space in freezer",
     *      description="Returns success",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreFreezerBookingRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FreezerBookingResource")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * )
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreFreezerBookingRequest $request
     * @param \App\Services\FreezerBookingService $blockBookingsRepository
     * @return FreezerBookingResource
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     */
    public function store(StoreFreezerBookingRequest $request, FreezerBookingService $blockBookingsRepository)
    {
        $newBooking = $blockBookingsRepository->addNewBookingBy(
            new CreateFreezerBookingRequestDto($request->input()),
            Auth::user()
        );

        return FreezerbookingResource::make($newBooking);
    }

    /**
     * @OA\Patch(
     *      path="/api/v1/bookings/{id}",
     *      operationId="updateBooking",
     *      tags={"Booking"},
     *      summary="Update booking record",
     *      description="Returns success",
     *      @OA\Parameter(in="path", name="id", description="ID of booking that you want to update", required=true, example=12, @OA\Schema(type="integer")),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FreezerBookingResource")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     * )
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateFreezerBookingRequest $request
     * @param \App\Models\FreezerBooking $freezerBooking
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFreezerBookingRequest $request, FreezerBooking $freezerBooking)
    {
        //
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/bookings/{id}",
     *      operationId="deleteBooking",
     *      tags={"Booking"},
     *      summary="Update booking record",
     *      description="Returns success",
     *      @OA\Parameter(in="path", name="id", description="ID of booking that user wants to delete", required=true, example=12, @OA\Schema(type="integer")),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     * )
     *
     * @param \App\Models\FreezerBooking $freezerBooking
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreezerBooking $freezerBooking)
    {
        //
    }
}
