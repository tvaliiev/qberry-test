<?php

namespace App\Http\Controllers;

use App\Models\FreezerRoomBooking;

class FreezerBlockBookingsController extends Controller
{
    public function index()
    {
        //
    }


    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\FreezerRoomBooking $freezerBlockBookings
     * @return \Illuminate\Http\Response
     */
    public function show(FreezerRoomBooking $freezerBlockBookings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\FreezerRoomBooking $freezerBlockBookings
     * @return \Illuminate\Http\Response
     */
    public function edit(FreezerRoomBooking $freezerBlockBookings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateFreezerBlockBookingsRequest $request
     * @param \App\Models\FreezerRoomBooking $freezerBlockBookings
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFreezerBlockBookingsRequest $request, FreezerRoomBooking $freezerBlockBookings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\FreezerRoomBooking $freezerBlockBookings
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreezerRoomBooking $freezerBlockBookings)
    {
        //
    }
}
