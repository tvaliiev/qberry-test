<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFreezerRoomRequest;
use App\Http\Requests\UpdateFreezerRoomRequest;
use App\Models\FreezerRoom;

class FreezerRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFreezerRoomRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFreezerRoomRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FreezerRoom  $freezerRoom
     * @return \Illuminate\Http\Response
     */
    public function show(FreezerRoom $freezerRoom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FreezerRoom  $freezerRoom
     * @return \Illuminate\Http\Response
     */
    public function edit(FreezerRoom $freezerRoom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFreezerRoomRequest  $request
     * @param  \App\Models\FreezerRoom  $freezerRoom
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFreezerRoomRequest $request, FreezerRoom $freezerRoom)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FreezerRoom  $freezerRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreezerRoom $freezerRoom)
    {
        //
    }
}
