<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateFreezerBookingRequest;
use App\Models\FreezerBooking;

class UserController
{
    /**
     * @OA\Get(
     *      path="/api/v1/users",
     *      operationId="listUsers",
     *      tags={"Users"},
     *      summary="List users",
     *      description="Returns success",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UsersResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     * )
     */
    public function index(){}
    /**
     * @OA\Post(
     *      path="/api/v1/users",
     *      operationId="createUser",
     *      tags={"Users"},
     *      summary="Create new user",
     *      description="Returns success",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreUserRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     * )
     * @return void
     */
    public function register(){}

    /**
     * @OA\Patch(
     *      path="/api/v1/users/{id}",
     *      operationId="updateUser",
     *      tags={"Users"},
     *      summary="Update user record",
     *      description="Returns success",
     *      @OA\Parameter(in="path", name="id", description="ID of user that you want to update", required=true, example=12, @OA\Schema(type="integer")),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateUserRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     * )
     * Update the specified resource in storage.
     *
     */
    public function update()
    {
        //
    }


    /**
     * @OA\Delete(
     *      path="/api/v1/users/{id}",
     *      operationId="deleteUser",
     *      tags={"Users"},
     *      summary="Delete specified user",
     *      description="Returns success",
     *      @OA\Parameter(in="path", name="id", description="ID of user to delete", required=true, example=12, @OA\Schema(type="integer")),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Unauthorized",
     *      ),
     * )
     *
     */
    public function destroy(FreezerBooking $freezerBooking)
    {
        //
    }
}
