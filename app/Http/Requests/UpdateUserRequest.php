<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store Freezer Booking request",
 *     @OA\Property(
 *         title="name",
 *         property="name",
 *         description="Username",
 *         example="John Snow"
 *     ),
 *     @OA\Property(
 *         title="email",
 *         property="email",
 *         description="Email",
 *         format="email",
 *         example="john@example.com"
 *     ),
 *     @OA\Property(
 *         title="password",
 *         property="password",
 *         description="password",
 *         example="pass123"
 *     ),
 * )
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
