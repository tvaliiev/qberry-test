<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 * @OA\Schema(
 *      title="Store Freezer Booking request",
 *      @OA\Property(
 *          property="temperature",
 *          title="temperature",
 *          description="Needed temperature for freezer in Celsius",
 *          format="int64",
 *          example="-20"
 *      ),
 *      @OA\Property(
 *          property="location_id",
 *          title="location_id",
 *          description="Location in",
 *          format="int64",
 *          example="15"
 *      ),
 *      @OA\Property(
 *          property="duration",
 *          title="duration",
 *          description="Duration of booking in days",
 *          format="int64",
 *          example="15"
 *      ),
 *      @OA\Property(
 *          property="start_date",
 *          title="start_date",
 *          description="Start date of booking",
 *          format="date",
 *          example="2022-06-17"
 *      ),
 *      @OA\Property(
 *          property="area",
 *          description="Area that you want to book",
 *          format="int64",
 *          example="10"
 *      ),
 * )
 */
class UpdateFreezerBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
