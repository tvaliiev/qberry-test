<?php

namespace App\Http\Requests;

use App\Models\Location;
use App\Rules\FreezerHasEnoughSpaceRule;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store Freezer Booking request",
 *      required={"temperature", "duration", "location_id", "area", "start_date"},
 *      @OA\Property(
 *          property="temperature",
 *          title="temperature",
 *          description="Needed temperature for freezer in Celsius",
 *          format="int64",
 *          example="-20"
 *      ),
 *      @OA\Property(
 *          property="location_id",
 *          title="location_id",
 *          description="Location in",
 *          format="int64",
 *          example="15"
 *      ),
 *      @OA\Property(
 *          property="duration",
 *          title="duration",
 *          description="Duration of booking in days",
 *          format="int64",
 *          example="15"
 *      ),
 *      @OA\Property(
 *          property="start_date",
 *          title="start_date",
 *          description="Start date of booking",
 *          format="date",
 *          example="2022-06-17"
 *      ),
 *      @OA\Property(
 *          property="area",
 *          description="Area that you want to book",
 *          format="int64",
 *          example="10"
 *      ),
 * )
 */
class StoreFreezerBookingRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'temperature' => ['required', 'lte:0'],
            'duration' => ['required', 'int', 'gt:0', 'lt:25'],
            'location_id' => ['required', 'exists:' . Location::class . ',id'],
            'area' => ['required', 'int', 'gt:0'],
            'start_date' => [
                'bail',
                'required',
                'date_format:' . DEFAULT_DATE_FORMAT,
                'after_or_equal:' . Carbon::now()->format(DEFAULT_DATE_FORMAT),
                new FreezerHasEnoughSpaceRule(),
            ],
        ];
    }
}
