<?php

namespace App\Repositories;

use App\Dto\FreezerRoomBooking\CreateFreezerRoomBookingDto;
use App\Models\FreezerRoomBooking;

class FreezerRoomBookingRepository
{
    public function create(CreateFreezerRoomBookingDto $createFreezerRoomBookingDto): FreezerRoomBooking
    {
        return FreezerRoomBooking::create($createFreezerRoomBookingDto->toArray());
    }
}
