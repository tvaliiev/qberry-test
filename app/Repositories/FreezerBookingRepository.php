<?php

namespace App\Repositories;

use App\Dto\FreezerBooking\CreateFreezerBookingDto;
use App\Dto\FreezerBooking\CreateFreezerBookingRequestDto;
use App\Dto\FreezerBooking\FreezerBookingDto;
use App\Models\FreezerBooking;
use Illuminate\Support\Str;

class FreezerBookingRepository
{
    public function create(CreateFreezerBookingDto $freezerBooking): FreezerBooking
    {
        return  FreezerBooking::create($freezerBooking->toArray());
    }
}
