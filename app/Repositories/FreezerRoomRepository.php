<?php

namespace App\Repositories;

use App\Models\FreezerRoom;
use Illuminate\Database\Eloquent\Builder as QBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FreezerRoomRepository
{
    public function getList(
        int    $locationId,
        array  $temperatures,
        string $startDate,
        string $endDate,
    ): Collection
    {
        return FreezerRoom::whereLocationId($locationId)
            ->whereBetween('temperature', $temperatures)
            ->with('freezerBlockBookings', function (HasMany $query) use ($startDate, $endDate) {
                $query->where(function (QBuilder $query) use ($startDate, $endDate) {
                    $query
                        ->whereBetween('start_date', [$startDate, $endDate])
                        ->orWhereBetween('end_date', [$startDate, $endDate]);
                });
            })
            ->get();
    }
}
