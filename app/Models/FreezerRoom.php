<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FreezerRoom extends Model
{
    use HasFactory;

    public const BLOCK_AREA = 2;

    public static function convertAreaToBlocksCount(int $area): int
    {
        return $area / static::BLOCK_AREA + $area % static::BLOCK_AREA;
    }
    public function freezerBlockBookings(): HasMany
    {
        return $this->hasMany(FreezerRoomBooking::class);
    }
}
