<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @OA\Schema(
 *     title="User",
 *     description="User model",
 *     @OA\Property(
 *         title="id",
 *         property ="id",
 *         description="id",
 *         format="int64",
 *         example=1
 *     ),
 *     @OA\Property(
 *         title="name",
 *         property="name",
 *         description="Username",
 *         example="John Snow"
 *     ),
 *     @OA\Property(
 *         title="email",
 *         property="email",
 *         description="Email",
 *         format="email",
 *         example="john@example.com"
 *     ),
 *     @OA\Property(
 *         title="created_at",
 *         property="created_at",
 *         description="Creation date",
 *         format="datetime",
 *         type="string",
 *         example="2022-03-22 13:12"
 *     ),
 *      @OA\Property(
 *          property="updated_at",
 *          title="updated_at",
 *          description="Last update date",
 *          format="datetime",
 *          type="string",
 *          example="2022-03-22 13:12"
 *      ),
 * )
 */
class User extends Authenticatable
{
    /**
     */
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
