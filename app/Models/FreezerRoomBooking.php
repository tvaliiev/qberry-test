<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OpenApi\Annotations\Property;

/**
 * @OA\Schema(
 *     title="Booking room record for freezer",
 *     @OA\Property(
 *         property="id",
 *         title="id",
 *         format="int64",
 *         example=123
 *     ),
 *     @OA\Property(
 *         property="start_date",
 *         title="start_date",
 *         format="date",
 *         example="2022-05-16",
 *     ),
 *     @OA\Property(
 *         property="end_date",
 *         title="end_date",
 *         format="date",
 *         example="2022-05-17",
 *     ),
 *     @OA\Property(
 *         property="capacity",
 *         title="capacity",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="user_id",
 *         title="user_id",
 *         description="User that booked",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="freezer_room_id",
 *         title="freezer_room_id",
 *         description="Freezer room that holds this bookings",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="location_id",
 *         title="location_id",
 *         description="Location of room (this is just for optimizing queries)",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         title="created_at",
 *         format="datetime",
 *         example="2022-02-16 22:33",
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         title="updated_at",
 *         format="datetime",
 *         example="2022-02-26 22:33",
 *     ),
 * )
 */
class FreezerRoomBooking extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function room(): BelongsTo
    {
        return $this->belongsTo(FreezerRoom::class);
    }
}
