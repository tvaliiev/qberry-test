<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     schema="FreezerBookingPaymentStatus",
 *     type="string",
 *     enum={"payed", "unpayed"},
 * )
 * @OA\Schema(
 *     title="Booking record for freezer",
 *     @OA\Property(
 *         property="id",
 *         title="id",
 *         format="int64",
 *         example=123
 *     ),
 *     @OA\Property(
 *         property="user_code",
 *         title="user_code",
 *         format="string with length of 12",
 *         example="E84W3BiuKpAC"
 *     ),
 *     @OA\Property(
 *         property="start_date",
 *         title="start_date",
 *         format="date",
 *         example="2022-05-16",
 *     ),
 *     @OA\Property(
 *         property="end_date",
 *         title="end_date",
 *         format="date",
 *         example="2022-05-17",
 *     ),
 *     @OA\Property(
 *         property="capacity",
 *         title="capacity",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="user_id",
 *         title="user_id",
 *         description="User that booked",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="location_id",
 *         title="location_id",
 *         description="Location of room (this is just for optimizing queries)",
 *         format="int64",
 *         example=12
 *     ),
 *     @OA\Property(
 *         property="payment_amount",
 *         title="payment_amount",
 *         description="Amount of monet that user needs to pay",
 *         format="float",
 *         example=25.00
 *     ),
 *     @OA\Property(
 *         property="payment_status",
 *         title="payment_status",
 *         description="Payment status",
 *         ref="#/components/schemas/FreezerBookingPaymentStatus",
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         title="created_at",
 *         format="datetime",
 *         example="2022-02-16 22:33",
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         title="updated_at",
 *         format="datetime",
 *         example="2022-02-26 22:33",
 *     ),
 * )
 */
class FreezerBooking extends Model
{
    use HasFactory;

    public const PAYMENT_STATUS_PAYED = 'payed';
    public const PAYMENT_STATUS_UNPAYED = 'unpayed';

    protected $guarded = [];
}
