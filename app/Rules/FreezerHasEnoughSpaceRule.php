<?php

namespace App\Rules;

use App\Models\FreezerRoomBooking;
use App\Models\FreezerRoom;
use App\Models\Location;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Builder as QBuilder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class FreezerHasEnoughSpaceRule implements Rule, DataAwareRule
{
    private Location $location;
    private int $temperature;
    private int $area;
    private string $startDate;
    private string $endDate;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $neededBlocks = FreezerRoom::convertAreaToBlocksCount($this->area);
        $temperature = [
            $this->temperature - 2, min(0, $this->temperature + 2),
        ];
        $totalCapacity = (int)$this->location
            ->rooms()
            ->whereBetween('temperature', $temperature)
            ->sum('capacity');

        $occupiedBlocksForDate = FreezerRoomBooking::query()
            ->where(function (QBuilder $query) {
                $query
                    ->whereBetween('start_date', [$this->startDate, $this->endDate])
                    ->orWhereBetween('end_date', [$this->startDate, $this->endDate]);
            })
            ->join('freezer_rooms', 'freezer_rooms.id', 'freezer_room_bookings.freezer_room_id')
            ->whereBetween('freezer_rooms.temperature', $temperature)
            ->where('freezer_room_bookings.location_id', $this->location->getKey())
            ->sum('freezer_room_bookings.capacity');
        $freeBlocks = $totalCapacity - $occupiedBlocksForDate;

        return $freeBlocks >= $neededBlocks;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Not having enough room in freezers for this date';
    }

    public function setData($data)
    {
        $this->location = Location::whereId($data['location_id'])->firstOrFail();
        $this->temperature = $data['temperature'];
        $this->area = $data['area'];
        $this->startDate = $data['start_date'];
        $this->endDate = Carbon::createFromFormat(DEFAULT_DATE_FORMAT, $data['start_date'])
            ->addDays($data['duration'])
            ->format(DEFAULT_DATE_FORMAT);
    }
}
