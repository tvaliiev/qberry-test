<?php

namespace App\Services;

use App\Dto\FreezerBooking\CreateFreezerBookingDto;
use App\Dto\FreezerBooking\CreateFreezerBookingRequestDto;
use App\Dto\FreezerBooking\FreezerBookingDto;
use App\Dto\FreezerRoomBooking\CreateFreezerRoomBookingDto;
use App\Models\FreezerRoomBooking;
use App\Models\FreezerBooking;
use App\Models\FreezerRoom;
use App\Models\User;
use App\Repositories\FreezerBookingRepository;
use App\Repositories\FreezerRoomBookingRepository;
use App\Repositories\FreezerRoomRepository;
use Illuminate\Database\Eloquent\Builder as QBuilder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FreezerBookingService
{
    public function __construct(
        protected FreezerBookingRepository     $freezerBookingRepository,
        protected FreezerRoomRepository        $freezerRoomRepository,
        protected FreezerRoomBookingRepository $freezerRoomBookingRepository,
    )
    {
    }

    public function addNewBookingBy(CreateFreezerBookingRequestDto $data, User $user): FreezerBookingDto
    {
        return DB::transaction(function () use ($data, $user) {
            $neededBlocks = FreezerRoom::convertAreaToBlocksCount($data->area);
            $endDate = Carbon::createFromFormat(DEFAULT_DATE_FORMAT, $data->start_date)
                ->addDays($data->duration)
                ->format(DEFAULT_DATE_FORMAT);
            $temperatures = [
                $data->temperature - 2, min(0, $data->temperature + 2),
            ];
            $booking = $this->freezerBookingRepository->create(new CreateFreezerBookingDto([
                'user_id' => $user->getKey(),
                'location_id' => $data->location_id,
                'start_date' => $data->start_date,
                'end_date' => $endDate,
                'capacity' => $neededBlocks,
                'user_code' => Str::random(12),
            ]));

            $this->freezerRoomRepository
                ->getList($data->location_id, $temperatures, $data->start_date, $endDate)
                ->each(function (FreezerRoom $room) use ($user, $data, $endDate, &$neededBlocks, $booking) {
                    // Get free blocks count for each room
                    $alreadyOccupiedBlocksCount = $room->freezerBlockBookings->sum->capacity;
                    /** @var FreezerRoom $room */
                    $room->freeBlocks = $room->capacity - $alreadyOccupiedBlocksCount;

                    // For each room that has free blocks, occupy them
                    if ($room->freeBlocks === 0) {
                        return true;
                    }

                    if ($room->freeBlocks === null) {
                        $room->freeBlocks = $room->capacity;
                    }

                    $this->freezerRoomBookingRepository->create(new CreateFreezerRoomBookingDto([
                        'user_id' => $user->getKey(),
                        'capacity' => min($neededBlocks, $room->freeBlocks),
                        'start_date' => $data->start_date,
                        'end_date' => $endDate,
                        'freezer_room_id' => $room->getKey(),
                        'location_id' => $data->location_id,
                        'freezer_booking_id' => $booking->getKey(),
                    ]));

                    $neededBlocks -= $room->freeBlocks;
                    return $neededBlocks > 0;
                });

            return new FreezerBookingDto($booking->toArray());
        });
    }

}
