<?php

use App\Http\Controllers\FreezerBlockBookingsController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => '/v1',
], static function () {
    Route::group([
        'middleware' => 'auth:sanctum',
    ], static function () {
        Route::post('/bookings', [\App\Http\Controllers\FreezerBookingController::class, 'store']);
    });

    Route::get('/token', function (){
        $token = User::query()->first()->createToken('Test token');

        return ['token' => $token->plainTextToken];
    });
});
